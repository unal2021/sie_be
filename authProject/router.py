from authApp.views.estudianteView import EstudianteViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('estudiantes', EstudianteViewset)