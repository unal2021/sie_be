from django.db import models

class Asignatura(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Nombre', max_length=30)
    grade = models.IntegerField('Grado', max_length=30)
