from django.db import models

# Create your models here.
class Estudiante(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField('FirstName', max_length=30)
    last_name = models.CharField('LastName', max_length=30)
    email = models.EmailField('Email', max_length=100)