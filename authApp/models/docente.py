from django.db import models

from .user import User

class Docente(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='docente', on_delete=models.CASCADE)
    lastChangeDate = models.DateTimeField()
    phone = models.CharField('Telefono', max_length=20) 
    degree = models.CharField('Profesión', max_length=20)
    address = models.CharField('Dirección', max_length=30)
    isActive = models.BooleanField(default=True)