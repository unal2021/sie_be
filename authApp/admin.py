from django.contrib import admin

# Register your models here.
from .models.user import User
from authApp.models import Estudiante
from .models.docente import Docente


admin.site.register(User)
admin.site.register(Estudiante)
admin.site.register(Docente)
