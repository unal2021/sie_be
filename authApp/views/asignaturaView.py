from rest_framework import viewsets
from authApp.models.asignatura import Asignatura
from authApp.serializers.asignaturaSerializer import AsignaturaSerializer

class AsignaturaViewset(viewsets.ModelViewSet):
    queryset = Asignatura.objects.all()
    serializer_class = AsignaturaSerializer