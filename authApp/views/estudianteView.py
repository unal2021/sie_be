from rest_framework import viewsets
from authApp.models.estudiante import Estudiante
from authApp.serializers.estudianteSerializer import EstudianteSerializer

class EstudianteViewset(viewsets.ModelViewSet):
    queryset = Estudiante.objects.all()
    serializer_class = EstudianteSerializer