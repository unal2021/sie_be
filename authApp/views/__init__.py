from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .estudianteView import EstudianteViewset
from .docenteView import DocenteViewset
from .asignaturaView import AsignaturaViewset
