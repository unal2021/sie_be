from rest_framework import viewsets
from authApp.models.docente import Docente
from authApp.serializers.docenteSerializer import DocenteSerializer

class DocenteViewset(viewsets.ModelViewSet):
    queryset = Docente.objects.all()
    serializer_class = DocenteSerializer
    