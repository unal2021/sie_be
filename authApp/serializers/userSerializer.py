from rest_framework import serializers
from authApp.models.user import User
from authApp.models.docente import Docente
from authApp.serializers.docenteSerializer import DocenteSerializer


class UserSerializer(serializers.ModelSerializer):
    docente = DocenteSerializer();

    class Meta:
        model = User
        fields = ['id', 'username', 'password',
                  'first_name', 'last_name', 'email', 'docente']

    def create(self, validated_data):
        docenteData = validated_data.pop('docente')
        userInstance = User.objects.create(**validated_data)
        Docente.objects.create(user=userInstance, **docenteData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        docente = Docente.objects.get(user=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'docente': {
                'id': docente.id,
                'lastChangeDate': docente.lastChangeDate,
                'isActive': docente.isActive,
                'phone': docente.phone,
                'degree': docente.degree,
                'address': docente.address,
            }
        }
