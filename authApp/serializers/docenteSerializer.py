from rest_framework import serializers
from authApp.models.docente import Docente


class DocenteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docente
        fields = ['lastChangeDate',
                  'phone',
                  'degree',
                  'address',
                  'isActive']
